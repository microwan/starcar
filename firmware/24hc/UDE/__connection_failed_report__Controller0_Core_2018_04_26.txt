----------------------------------------------------------

  Connection Failed Report from
  Basic UDE Target Interface, Version: 1.14.5
  created: 04/26/18, 16:13:41

----------------------------------------------------------

Windows version:
  Win7 (Service Pack 1)
  Admin: yes

UDE version:
  Release:  4.08.06
  Build:    8015
  Path:     C:\Program Files (x86)\pls\UDE Starterkit 4.8

Target configuration file:
  C:\Users\boyera\Desktop\CodeEquipe\gagnants\08-SWAG\RENDU\24h2017\SPC5\24hc\UDE\spc560d_discovery_debug_jtag.cfg

Error messages:
  PpcJtagTargIntf: Can't connect target !
  PpcJtagTargIntf: No JTAG client found !
Please check:
- target power supply
- JTAG cable connection
- target configuration

Settings:
  PortType:  CommDev
  CommDevSel:  PortType=USB,Type=FTDI
  JtagViaPod:  n
  TargetPort:  Default
  JtagTapNumber:  0
  JtagNumOfTaps:  1
  JtagNumIrBefore:  0
  JtagNumIrAfter:  0
  UseExtendedCanId:  n
  JtagOverCanIdA:  0x00000001
  JtagOverCanIdB:  0x00000002
  JtagOverCanIdC:  0x00000003
  JtagOverCanIdD:  0x00000004
  JtagOverCanIdE:  0x00000005
  JtagmTckSel:  3
  JtagmInterFrameTimer:  0
  MaxJtagClk:  2500
  AdaptiveJtagPhaseShift:  y
  JtagMuxPort:  -1
  JtagMuxWaitTime:  5
  JtagIoType:  Jtag
  EtksArbiterMode:  None
  EtksMicroSecondTimeout:  100
  CheckJtagId:  y
  ConnOption:  Default
  UseExtReset:  y
  SetDebugEnableAb1DisablePin:  n
  OpenDrainReset:  n
  ResetWaitTime:  50
  HaltAfterReset:  y
  ChangeJtagClk:  10000
  ExecInitCmds:  y
  InvalidateCache:  n
  ChangeMsr:  n
  ChangeMsrValue:  0x00000000
  ResetPulseLen:  10
  InitScript Script:
    // setup IVOPR
    // points to internal memory at 0x40000000
    SETSPR 0x3F 0x40000000 0xFFFFFFFF
    
    // disable watchdog
    SET SWT_SR 0xC520
    SET SWT_SR 0xD928
    SET SWT_CR 0xFF00000A
    
    // Oscillator select
    SET CGM_OCDS_SC 0x1000000
    SET CGM_OC_EN 0x1
    
    // enable all modes
    SET ME_MER 0x5FF
    
    // run mode
    SET ME_DRUN_MC 0x1F0032
    SET ME_RUN_PC0 0xFE
    
    // enable peripherals in run and low power modes
    SET ME_LP_PC0 0x500
    
    // enable clocks
    SET8 CGM_SC_DC0 0x80
    SET8 CGM_SC_DC1 0x80
    SET8 CGM_SC_DC2 0x80
    
    // setup clock monitor
    SET CMU_CSR 0x6
    SET CMU_LFREFR 0x1
    SET CMU_HFREFR 0xFFE
    
    // Make DRUN configuration active
    SET ME_MCTL 0x30005AF0
    SET ME_MCTL 0x3000A50F
    WAIT 0x5
    
    // setup pll to 48MHz
    SET FMPLL_CR 0x5300041 0xFFFFFFFF
    // run mode
    SET ME_DRUN_MC 0x1F00F4
    
    // Make DRUN configuration active
    SET ME_MCTL 0x30005AF0
    SET ME_MCTL 0x3000A50F
    WAIT 0x5
    
    // setup SSCM erro cfg for debug
    SET16 SSCM_ERROR 0x3 0x3
    
  ExecOnConnectCmds:  n
  OnConnectScript Script:
    Script is empty
  SimioAddr:  g_JtagSimioAccess
  FreezeTimers:  y
  AllowMmuSetup:  y
  ExecOnStartCmds:  n
  OnStartScript Script:
    
  ExecOnHaltCmds:  n
  ExecOnHaltCmdsWhileHalted:  n
  OnHaltScript Script:
    Script is empty
  EnableProgramTimeMeasurement:  n
  TimerForPTM:  Default
  DefUserStreamChannel:  0
  DontUseCachedRegisters:  n
  AllowBreakOnUpdateBreakpoints:  n
  ClearDebugStatusOnHalt:  y
  UseRestartWhileRunningHandling:  n
  DoNotEnableSwBrk:  n
  TargetAppHandshakeMode:  None
  TargetAppHandshakeTimeout:  100
  TargetAppHandshakeParameter0:  0x00000000
  TargetAppHandshakeParameter1:  0x00000000
  TargetAppHandshakeParameter2:  0x00000000
  TargetAppHandshakeParameter3:  0x00000000
  UseNexus:  y
  DoSramInit:  y
  ForceCacheFlush:  n
  IgnoreLockedLines:  n
  HandleWdtBug:  n
  ForceEndOfReset:  n
  UseHwResetMode:  n
  HwResetMode:  Simulate
  HandleNexusAccessBug:  n
  UseMasterNexusIfResetState:  y
  UseLocalAddressTranslation:  y
  Use64BitNexus:  n
  InitSramOnlyWhenNotInitialized:  n
  AllowHarrForUpdateDebugRegs:  n
  HaltOnDnh:  y
  AlwaysHaltOnDni:  y
  EnableLowPowerDebugHandshake:  n
  EnableLockstepDebug:  y
  AddBranchBeforeGo:  n
  InvalidTlbOnReset:  n
  DoNotEnableTrapSwBrp:  n
  AllowResetOnCheck:  n
  BootPasswd0:  0xFEEDFACE
  BootPasswd1:  0xCAFEBEEF
  BootPasswd2:  0xFFFFFFFF
  BootPasswd3:  0xFFFFFFFF
  BootPasswd4:  0xFFFFFFFF
  BootPasswd5:  0xFFFFFFFF
  BootPasswd6:  0xFFFFFFFF
  BootPasswd7:  0xFFFFFFFF
  PasswordFile:  
  UsePasswordForUnlockDevice:  y
  DisableE2EECC:  n
  UseCore0ForNexusMemoryAccessWhileRunning:  n
  ForceDniForDebugger:  n
  HandleOvRamInitViaNarWorkaround:  n
  ApplySPC58NE84XoscWorkaround:  y
  ApplyEigerEdJtagWorkaround:  n
  IsUsedByTester:  n
  Mpc57xxClearPeripheralDebugAtNextCheckUserAppWhenRunning:  n
  SlaveHasHalted:  n

JTAG target infos:
  JTAG-ID:           0x00000000
  UsedJtagClk:       2000 kHz
  ExtVoltage:        0.0 V
  IntVoltageUsed:    n

Target infos:
  CoreName:  Core
  FullCoreName:  Controller0.Core
  ExtClock:  8000000
  IntClock:  48000000
  SysClock:  0
  StmClock:  0
  AccessToken:  0x443B
  HasNexus:  n
  BigEndian:  n
  CanSimio:  n
  CanPhysicalAccess:  n
  HasSpe:  n
  NumOfSimioChannels:  0
  JtagId:  0x00000000
  IsEarlyStep:  n
  IsMaster:  y
  MasterCoreName:  
  IsMasterEnabled:  y
  IsSlave:  n
  BuddyDeviceDetected:  n
  EtkConnected:  n
  Data TLB size on target:  0x00000000
  Instruction TLB size on target:  0x00000000
  Shared TLB size on target:  0x00000000
  Number of data TLB entries:  0x00000000
  Number of instruction TLB entries:  0x00000000
  Number of shared TLB entries:  0x00000000
  Extended E200 MMU:  n
  E200 MPU:  n
  Data cache size:  0x00000000
  Data cache ways:  0x00000000
  Data cache sets:  0x00000000
  Data cache entry size:  0x00000000
  Instruction cache size:  0x00000000
  Instruction cache ways:  0x00000000
  Instruction cache sets:  0x00000000
  Instruction cache entry size:  0x00000000
  Unified Cache:  n
  MCM base address:  0xFFF40000
  SIU base address:  0xC3F90000
  Nexus On Slave:  n
  Core Number:  0
  Has Wdt bug:  n
  Length of IR register:  0x00000005
  Has Data Value comparators:  n
  Reset Mode:  0x00000003
  STM timer base address:  0xFFF3C000
  MC_ME base address:  0xC3FDC000
  Core in Lockstep mode:  n
  Core in DPM mode:  n
  Core is HSM:  n
  Core is Master of HSM:  n
  Name of other Master Core:  
  HsmBootEnabled:  n
  Target has Nexus access bug:  n
  Target has unlock JTAG capability:  y
  Unlock JTAG password len:  0x00000040
  Has JTAG unlock enable bit:  y
  ExecuteOpcodeAddr:  0xFFFFC000
  IMEMBaseAddr:  0xFFFFFFFF
  IMEMSize:  0x00000000
  DMEMBaseAddr:  0xFFFFFFFF
  DMEMSize:  0x00000000
  BootCodeStartAddr:  0xFFFFFFFF
  HasCJtag:  n
  HasLfast:  n
  HasJtagOverCan:  n
  HasNpcLowPowerHandshake:  y
  HasLockstepDebug:  n
  TargetIsForSpc5UdeStk:  n
  PllCalc:  UDE.SPC560BPllCalc
  JtagIdWhiteList:  
  JtagIdBlackList:  
  DciPinControl:  
  DciControl:  
  ChipJtagTapNumber:  0x00000000
  ChipJtagTapNumber:  0x00000001
  ChipJtagTapNumber:  0x00000000
  ChipJtagTapNumber:  0x00000000
  JtagChainType:  UNKNOWN
  JtagChainNumber:  0x00000000
  PowerPc system type:  MPC56XX
  PowerPc synchonized GO type:  NONE
  InactiveAfterReset:  n
  NumOfActiveCores:  1

Communication device:
  Type/Firmware:  FtdiCommDev V1.1.8
  Serial Number:  15416

Communication protocol handler:
  LastCmd:      0x0410
  LastResult:   0xC000
  ExpBytes:     580 (0x0244)
  RetBytes:     580 (0x0244)
  LastTimeout:  120020

Protocol diagnostic output:
  LastJtagApiAddr:   0x00000000
  LastJtagApiSpr:    0x00000000
  LastJtagApiDcr:    0x00000000
  LastJtagApiError:  0x00000000
  LastJtagApiStatus: 0x00000000
  JtagApiErrorLine:  1290
  JtagApiAddInfo0:   0x00000000
  JtagApiAddInfo1:   0x00000000
  ProtErrorLine:     5116
  LowLevelDiag0:     DEADBEEF (-559038737)
  LowLevelDiag1:     DEADBEEF (-559038737)
  LowLevelDiag2:     DEADBEEF (-559038737)
  LowLevelDiag3:     DEADBEEF (-559038737)


