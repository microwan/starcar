/*
 * point.h
 *
 *  Created on: 21 janv. 2017
 *      Author: Mehdi
 */

#ifndef POINT_H_
#define POINT_H_

struct point{
	int gear;
	int32_t lat, lng;
};

#endif /* POINT_H_ */
