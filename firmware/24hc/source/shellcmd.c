/*
 * Licensed under ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal.h"
#include "ch.h"
#include "shell.h"
#include "chprintf.h"
#include "shellcmd.h"
#include "shellbt.h"
#include "serial_input.h"
#include "speedsensor.h"
#include "carcommand.h"
#include "point.h"
#include "atan.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>

int gear = 0;
int iCarBlinker = 0;
bool_t bCarWipers = false;
bool_t bFullDemoRunning = false;
bool_t headLights = false;
bool_t tailLights = false;
bool_t gps = false;
int32_t lat;
int32_t lng;
struct point points[256];

extern bool_t emergency;
extern bool_t rain;
static int i = 0;

int sleepDelay(){
	if(rain){
		switch (gear){
			case -1:return 1750;
			case 0: return 1000;
			case 1: return 2000;
			case 2: return 1500;
			case 3: return 1000;
		}
	}
	else{
		switch (gear){
			case -1:return 1250;
			case 0: return 1000;
			case 1: return 1500;
			case 2: return 1000;
			case 3: return 500;
		}
	}
}

static WORKING_AREA(waThreadLocation, 128);
static msg_t ThreadLocation(void *arg) {
	(void)arg;
	chRegSetThreadName("ThreadLocation");
	int j = 0;
	while(gps){
		if(!emergency){
			j++;
			gear = points[j].gear;
			motorcontrol();
			lat = points[j].lat;
			lng = points[j].lng;
			j = j % (i-1);
		}
		osalThreadSleepMilliseconds(sleepDelay());
	}
	chThdExit(0);
	return 0;
}


/*
 * LEDs blinker thread, times are in milliseconds.
 */
WORKING_AREA(waThreadCarBlinker, 128);
msg_t ThreadCarBlinker(void *arg) {
	(void)arg;
	chRegSetThreadName("ThreadCarBlinker");

	while (true) {
		if (iCarBlinker == 1) {
			palTogglePad(PORT_A, PA_LEDJAUNE1);
		}
		else if (iCarBlinker == 2) {
			palTogglePad(PORT_A, PA_LEDJAUNE2);
		}
		else if (iCarBlinker == 3) {
			palTogglePad(PORT_A, PA_LEDJAUNE2);
			palTogglePad(PORT_A, PA_LEDJAUNE1);
		}
		else {
			palClearPad(PORT_A, PA_LEDJAUNE1);
			palClearPad(PORT_A, PA_LEDJAUNE2);
		}

		osalThreadSleepMilliseconds(500);
	}

	palClearPad(PORT_A, PA_LEDJAUNE1);
	palClearPad(PORT_A, PA_LEDJAUNE2);

	chThdExit(0);

	return 0;
}

/*position*/
WORKING_AREA(waThreadCarDirection, 128);
msg_t ThreadCarDirection(void *arg) {
	(void)arg;
	chRegSetThreadName("ThreadCarDirection");
	/*int compteur = 0;
	int post;
	int ant;
	float ant_angle, post_angle;
	float res;
	struct point p_ant;
	struct point p_now;
	struct point p_post;

	while(true){
		p_now = points[compteur];
		if(compteur == 0)
		{
			p_ant = points[i];
			p_post = points[(compteur+1)];
		} else if (compteur == i)
		{
			p_post = points[0];
			p_ant = points[(compteur-1)];
		}
		else
		{
			p_ant = points[(compteur-1)];
			p_post = points[(compteur+1)];
		}
		p_ant.lat -= p_now.lat;
		p_post.lat -= p_now.lat;
		p_ant.lng -= p_now.lng;
		p_post.lng -= p_now.lng;

		ant = (p_ant.lat/p_ant.lng);
		post = (p_post.lat/p_post.lng);
		if (ant < -10){
			ant_angle = arctan[0];
		}
		else if (ant > 10){
			ant_angle = arctan[199];
		}
		else{
			ant_angle = arctan[(ant + 10) * 10];
		}

		if (post < -10){
			post_angle = arctan[0];
		}
		else if (ant > 10){
			post_angle = arctan[199];
		}
		else{
			post_angle = arctan[(post + 10) * 10 ];
		}

		ant_angle = arctan[(int)((p_ant.lat/p_ant.lng)*255-128)];
		post_angle = arctan[(int)((p_post.lat/p_post.lng)*255-128)];
		res = (post_angle - ant_angle) * 360 / 3.14159;
		if(res >= -180 && res <180);
		else if (res < -180)
		{
			res += 360;
		}
		else{
			res -= 360;
		}
		if(res !=-180 || res !=0)
		{
			if(res>0)
			{
				turncmd(2);
			}
			else
			{
				turncmd(1);
			}
		}
		else if(res == -180){
			turncmd(2);
		}
		else
		{
			turncmd(0);
		}
		if(compteur < i)
		{
			compteur ++;
		}
		else
		{
			compteur = 0;
		}
		osalThreadSleepMilliseconds(400);
	}*/
}


/*
 * LEDs blinker thread, times are in milliseconds.
 */
WORKING_AREA(waThreadCarWipers, 128);
msg_t ThreadCarWipers(void *arg) {
	(void)arg;
	chRegSetThreadName("ThreadCarWipers");

	while (true) {
		if (bCarWipers) {
			palTogglePad(PORT_C, PC_LEDBLEU);
		}
		else {
			palClearPad(PORT_C, PC_LEDBLEU);
		}
		osalThreadSleepMilliseconds(1000);
	}

	palClearPad(PORT_C, PC_LEDBLEU);

	chThdExit(0);

	return 0;
}
/*
 * Full Demo thread, times are in milliseconds.
 */
static WORKING_AREA(waThreadFullDemo, 128);
static msg_t ThreadFullDemo(void *arg) {
	(void)arg;
	chRegSetThreadName("ThreadFullDemo");

	while (bFullDemoRunning) {

		/* Forward during 5 s*/
		if (bFullDemoRunning)
			drivecmd(1, 5000);
		else {
			stopcmd();
			break;
		}

		osalThreadSleepMilliseconds(5000);

		/* Reverse during 5 s*/
		if (bFullDemoRunning)
			drivecmd(2, 5000);
		else
			break;

		osalThreadSleepMilliseconds(5000);

		/* Turn Left*/
		if (bFullDemoRunning)
			turncmd(1);
		else {
			stopcmd();
			break;
		}

		osalThreadSleepMilliseconds(5000);

		/* Turn Right*/
		if (bFullDemoRunning)
			turncmd(2);
		else {
			stopcmd();
			break;
		}

		osalThreadSleepMilliseconds(5000);

		/* Straight Away*/
		if (bFullDemoRunning)
			turncmd(0);
		else {
			stopcmd();
			break;
		}

		osalThreadSleepMilliseconds(5000);

		/* Stop all the motors */
		if (bFullDemoRunning)
			stopcmd();
		else {
			stopcmd();
			break;
		}

		/* Blink the car */
		if (bFullDemoRunning) {
			bLightOn = true;
		}
		else {
			stopcmd();
			break;
		}
		osalThreadSleepMilliseconds(20000);
		bLightOn = false;
	}

	chThdExit(0);

	return 0;
}

/*
 * Memory statistics.
 */
void cmd_mem(BaseSequentialStream *chp, int argc, char *argv[]) {
	size_t n, size;

	(void)argv;
	if (argc > 0) {
		chprintf(chp, "Usage: mem\r\n");
		return;
	}
	n = chHeapStatus(NULL, &size);
	chprintf(chp, "core free memory : %u bytes\r\n", chCoreStatus());
	chprintf(chp, "heap fragments   : %u\r\n", n);
	chprintf(chp, "heap free total  : %u bytes\r\n", size);
}

/*
 * List of threads.
 */
void cmd_threads(BaseSequentialStream *chp, int argc, char *argv[]) {
	static const char *states[] = {THD_STATE_NAMES};
	Thread *tp;

	(void)argv;
	if (argc > 0) {
		chprintf(chp, "Usage: threads\r\n");
		return;
	}
	chprintf(chp, "    addr    stack prio refs     state time\r\n");
	tp = chRegFirstThread();
	do {
		chprintf(chp, "%.8lx %.8lx %4lu %4lu %9s %lu\r\n", (uint32_t)tp,
				(uint32_t)tp->p_ctx.sp, (uint32_t)tp->p_prio,
				(uint32_t)(tp->p_refs - 1), states[tp->p_state],
				(uint32_t)tp->p_time);
		tp = chRegNextThread(tp);
	} while (tp != NULL);
}

/*
 * Send a AT Command
 */
static void cmd_at(BaseSequentialStream *chp, int argc, char *argv[]) {

	char pval[30];

	(void)argv;
	if (argc == 0) {
		chprintf(chp, "Usage: send <ATCOMMAND>\r\n");
		chprintf(chp,
				"WARNING : BT device should not be connected on any terminal\r\n");
		return;
	}

	get_string(argv[0], pval, 1000);
	chprintf(chp, pval);
	chprintf(chp, "\r\n");
}

/*
 * Start GPS Frames
 */
void startgps(BaseSequentialStream *chp, int argc, char *argv[]) {

	(void)argv;
	if (argc > 0) {
		chprintf(chp, "Usage: startgps\r\n");
		return;
	}
	chThdCreateStatic(waThreadLocation, sizeof(waThreadLocation),
							NORMALPRIO,
							ThreadLocation, NULL);
	gps = true;
}

/*
 * Stop GPS Frames
 */
void stopgps(BaseSequentialStream *chp, int argc, char *argv[]) {

	(void)argv;
	if (argc > 0) {
		chprintf(chp, "Usage: stopgps\r\n");
		return;
	}

	gps = false;
}

/*
 * Display Speed Counter
 */
void speed(BaseSequentialStream *chp, int argc, char *argv[]) {

	(void)argv;
	if (argc > 0) {
		chprintf(chp, "Usage: speed\r\n");
		return;
	}

	/*
	 * Display the Speed Counter.
	 */
	chprintf(chp, "Speed %d\r\n", counter);
}

/*
 * drive motor
 */
void drive(BaseSequentialStream *chp, int argc, char *argv[]) {

	int speed = 0;
	int direction = 0;

	(void)argv;
	if (argc != 2) {
		chprintf(chp, "Usage: drive <DIRECTION (0/1/2)> <SPEED (0-10000)>\r\n");
		return;
	}

	direction = atoi(argv[0]);
	speed = atoi(argv[1]);

	if ((speed < 0) || (speed > 10000)) {
		chprintf(chp, "Please between 0 and 10000\r\n");
	}
	else {
		drivecmd(direction, speed);
	}

}

/*
 *  turn
 */
void turn(BaseSequentialStream *chp, int argc, char *argv[]) {

	int direction = 0;

	(void)argv;
	if (argc != 1) {
		chprintf(chp, "Usage: turn <DIRECTION (0/1/2)>\r\n");
		return;
	}

	direction = atoi(argv[0]);

	/* Call the Command turn*/
	turncmd(direction);
}

/*
 * blink the car
 */
void blink(BaseSequentialStream *chp, int argc, char *argv[]) {

	int direction = 0;

	(void)argv;
	if (argc == 0) {
		chprintf(chp, "Usage: blink <0/1>\r\n");
		return;
	}

	direction = atoi(argv[0]);

	if (direction) {
		bLightOn = true;
	}
	else {
		bLightOn = false;
	}
}

/*
 * full demo start spc5carsimu
 */
void startdemo(BaseSequentialStream *chp, int argc, char *argv[]) {

	(void)argv;
	if (argc != 0) {
		chprintf(chp, "Usage: startdemo\r\n");
		return;
	}

	if (bFullDemoRunning == false) {
		bFullDemoRunning = true;
		/*
		 * Creates the Full Demo thread. (it does not exist)
		 */
		chThdCreateStatic(waThreadFullDemo, sizeof(waThreadFullDemo), NORMALPRIO,
				ThreadFullDemo, chp);
	}
	else {
		chprintf(chp, "Full Demo Already running\r\n");
	}

}

/*
 * full demo start spc5carsimu
 */
void stopdemo(BaseSequentialStream *chp, int argc, char *argv[]) {

	(void)argv;
	if (argc != 0) {
		chprintf(chp, "Usage: stopdemo\r\n");
		return;
	}

	bFullDemoRunning = false;
}

void headlights(BaseSequentialStream *chp, int argc, char *argv[]) {

	(void)argv;
	if (argc != 1) {
		chprintf(chp, "Usage: headlights (1/0)\r\n");
		return;
	}
	if (atoi(argv[0]) == 1){
		palSetPad(PORT_B, PB_LEDBLANCHE);
		headLights = true;
	}
	else if (atoi(argv[0]) == 0){
		palClearPad(PORT_B, PB_LEDBLANCHE);
		headLights = false;
	}
}

void taillights(BaseSequentialStream *chp, int argc, char *argv[]) {

	(void)argv;
	if (argc != 1) {
		chprintf(chp, "Usage: taillights (1/0)\r\n");
		return;
	}
	if (atoi(argv[0]) == 1){
		palSetPad(PORT_C, PC_LEDROUGE);
		tailLights = true;
	}
	else if (atoi(argv[0]) == 0){
		palClearPad(PORT_C, PC_LEDROUGE);
		tailLights = false;
	}
}

void blinker(BaseSequentialStream *chp, int argc, char *argv[]) {
	(void)argv;
	if (argc != 1) {
		chprintf(chp, "Usage: blinker (0/1/2/3)\r\n");
		return;
	}
	if (atoi(argv[0]) == 1){
		palClearPad(PORT_A, PA_LEDJAUNE2);
		if (iCarBlinker == 0) {
			iCarBlinker = 1;
			/*
			 * Creates the car blinker thread. (it does not exist)
			 */
			chThdCreateStatic(waThreadCarBlinker, sizeof(waThreadCarBlinker),
					NORMALPRIO,
					ThreadCarBlinker, NULL);
		}
		else {
			iCarBlinker = 1;
		}
	}
	else if (atoi(argv[0]) == 2){
		palClearPad(PORT_A, PA_LEDJAUNE1);
		if (iCarBlinker == 0) {
			iCarBlinker = 2;
			/*
			 * Creates the car blinker thread. (it does not exist)
			 */
			chThdCreateStatic(waThreadCarBlinker, sizeof(waThreadCarBlinker),
					NORMALPRIO,
					ThreadCarBlinker, NULL);
		}
		else {
			iCarBlinker = 2;
		}
	}
	else if (atoi(argv[0]) == 3){
		palClearPad(PORT_A, PA_LEDJAUNE1);
		palClearPad(PORT_A, PA_LEDJAUNE2);
		if (iCarBlinker == 0) {
			iCarBlinker = 3;
			/*
			 * Creates the car blinker thread. (it does not exist)
			 */
			chThdCreateStatic(waThreadCarBlinker, sizeof(waThreadCarBlinker),
					NORMALPRIO,
					ThreadCarBlinker, NULL);
		}
		else {
			iCarBlinker = 3;
		}
	}
	else if (atoi(argv[0]) == 0){
		iCarBlinker = 0;
	}
}

void wipers(BaseSequentialStream *chp, int argc, char *argv[]) {
	(void)argv;
	if (argc != 1) {
		chprintf(chp, "Usage: wipers (0/1)\r\n");
		return;
	}
	if (atoi(argv[0]) == 1){
		if (!bCarWipers) {
			bCarWipers = true;
		}
		else {
			bCarWipers = true;
		}
	}
	else if(atoi(argv[0]) == 0){
		bCarWipers = false;
	}
}

void status(BaseSequentialStream *chp, int argc, char *argv[]) {
	(void)argv;
	chprintf(chp, "headlights %d\r\n", (int)headLights);
	chprintf(chp, "taillights %d\r\n", (int)tailLights);
	chprintf(chp, "blinkers %d\r\n", iCarBlinker);
	chprintf(chp, "wipers %d\r\n", (int)bCarWipers);
	chprintf(chp, "speed %d\r\n", counter);
	chprintf(chp, "gear %d\r\n", gear);
	chprintf(chp, "lat %d\r\n", lat);
	chprintf(chp, "lng %d\r\n", lng);
	resetSpeedSensor();
}

void shift(BaseSequentialStream *chp, int argc, char *argv[]) {
	(void)argv;
	if(gear < 3){
		gear++;
		motorcontrol();
	}
}

void downshift(BaseSequentialStream *chp, int argc, char *argv[]) {
	(void)argv;
	if(gear > -1){
		gear--;
		motorcontrol();
	}
}

void setgear(BaseSequentialStream *chp, int argc, char *argv[]) {
	(void)argv;
	if (argc != 1) {
			chprintf(chp, "Usage: gear [-1 ... 3]\r\n");
			return;
	}
	int g = atoi(argv[0]);
	if (g <= 3 && g >=  -1){
		gear = g;
		motorcontrol();
	}
}

void flushPoints(void){
	memset(&points, 0, 256*sizeof(struct point));
	i = 0;
}

void point(BaseSequentialStream *chp, int argc, char *argv[]){
	if(i == 255)
		flushPoints();

	if (argc != 3) {
		chprintf(chp, "Usage: point <gear> <lat> <lng>\r\n");
		return;
	}
	points[i].gear = atoi(argv[0]);
	points[i].lat = atoi(argv[1]);
	points[i].lng = atoi(argv[2]);
	i++;
}

void flush(BaseSequentialStream *chp, int argc, char *argv[]){
	flushPoints();
}

void calculatedirection(BaseSequentialStream *chp, int argc, char *argv[]){
	chThdCreateStatic(waThreadCarDirection, sizeof(waThreadCarDirection),
							NORMALPRIO,
							ThreadCarDirection, NULL);
}

void printpoints(BaseSequentialStream *chp, int argc, char *argv[]) {
	(void)argv;
	int j;
	chprintf(chp, "%d points\r\n", i);
	for(j = 0; j < i; j++){
		chprintf(chp, "gear = %d | lat = %d | lng = %d\r\n", points[j].gear, points[j].lat, points[j].lng);
	}
}

void init(void){
	chThdCreateStatic(waThreadCarWipers, sizeof(waThreadCarWipers),
						NORMALPRIO,
						ThreadCarWipers, NULL);
	chThdCreateStatic(waThreadCarBlinker, sizeof(waThreadCarBlinker),
						NORMALPRIO,
						ThreadCarBlinker, NULL);

}
/*
 * Array of defined commands, you can add more.
 */
const ShellCommand shell_commands[] = { {"mem", cmd_mem}, {"threads",
		cmd_threads},
		{"send", cmd_at}, {"startgps", startgps},
		{"stopgps", stopgps}, {"speed", speed}, {
				"drive", drive},
				{"turn", turn}, {"blink", blink}, {
						"startdemo", startdemo},
						{"stopdemo", stopdemo}, {"headlights", headlights},
						{"taillights", taillights}, {"blinker", blinker},
						{"wipers", wipers}, {"status", status},
						{"shift", shift}, {"downshift", downshift},
						{"setgear", setgear}, {"point", point},
						{"flush", flush}, {"printpoints", printpoints},{"calculatedirection", calculatedirection},{NULL, NULL}};
