# STARCAR
This project is the result of the sequel of the 24 Hours coding 2017 "ST Amazing Race Car":

The annual event took place on 21 and 22 January at the CCI of Le Mans in Sarthe , France. ST Le Mans proposed this year the simulation of a car race. 
Ghislain Ballester, Michel Wojdaszka, Erwan Yvin and Philippe Noël have designed 10 "intelligent car" models that can react, be remotely controlled and become autonomous, using a microcontroller of the automotive family ADG SPC5 Studio.

I developed during my internship in STMicroelectronics, a bluetooth dashboard for android device.

### Description ###

3 steps for competitors: garage mode, sensor mode (semi-auto) and automatic control.

• Create a remote control, a dashboard either via Android (via bluetooth link) or by serial link on PC which can control the organs of the car in real time (leds and motors)
• Interact with external events (night, rain, emergency ...) in semi-automatic mode with sensors (simulated by pushbuttons)
• Communicate the position of the car via a GPS link on a virtual map and create an autopilot that follows a given trajectory.
After 24 hours of challenge, the team "The SWAG unicorns" won the prize.


Demonstration video
https://www.youtube.com/watch?v=4ftBosYKEIE

### What is SPC5Studio ? ###

You can integrate some components in SPC5Studio tool.
Release SPC5-HAL branches are already available in the SPC5Studio Marketplace / SPC5Studio Update Site.

SPC5Studio provides a framework to easily design, build and deploy applications for SPC56 Power Architecture 32-bit Automotive Microcontrollers.
Combining a project editor, a sophisticated code generator, a dedicated HighTec GNU "C" compiler, a PLS starter kit debugger and several software elements such as code examples, low level drivers and libraries.

Configuration complexity has been organized in a simple way of setup assistant that drives the customer through well defined steps, by using visual valuable wizards.

SPC5Studio is an open framework to host several software packages and functionalities to simplify developers evaluation and application start-up.
It offers a free, eclipse based, fully integrated development environment with full flexibility to integrate functional extension; dedicated eclipse plug-ins inside SPC5Studio wrap software packages and libraries into Solutions

Includes: Application examples selection Wizard, Pin Map Graphic Wizard, New Project Wizard, New Component Wizard.
Embedded Software is available including Register Level Access, Platform components, Application Example, SW Libraries, RTOS, HAL.
SPC5Studio offers also pre-integrated solutions for compiler and debugger and support is granted by Help-On-Line solution.
To know more and download the SPC5Studio latest version visit ST dedicated WEB page:

* Download [SPC5Studio (http://www.spc5studio.com/)](http://www.spc5studio.com)

### Getting Started ###
-You have to flash the firmware code located in firmware directory into the board using SPC5Studio.
-Afterwards you may install the apk on an android device with bluetooth (and gyroscope for better usage but not mandatory).
-Then you can proceed to connect your device with the board and enjoy remotely driving Your car.

### Demo ###


### Files Organization ###
--{root}                  - STCar directory.
  +--README.md            - This file.
  +--android / 			  - Bluetooth remote dashboard directory.
  +--binary/              - compiled STCar application.
  +--documents/           - Some documentations.
  +--firmware/            - source firmware directory & binary.
  +--montage/             - documents on how to perform the montage.
  

### Authors ###
-Antoine BOYER a.boyer@iia-laval.fr :Android Application.
-Swag Team : Firmware developed in SPC5Studio.

### Acknowledgments ###
-Omar Bluetooth connection https://github.com/OmarAflak/Bluetooth-Terminal 
