package com.spc5studio.boyera.STCar;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.Semaphore;

import me.aflak.STCar.R;
import me.aflak.bluetooth.Bluetooth;


// This class implements all the needed to communicate with the board once they are connected
// It's called when  a device is selected from the first layout

public class Chat extends AppCompatActivity implements Bluetooth.CommunicationCallback, SensorEventListener {
    //<editor-fold desc="Initialization ">
    static public boolean isDebug = false;

    public static final int DECELERATION_BRAKE = -20;
    public static final String CR = "\n\r";
    public static final String HEADLIGHTS_ON = "headlights 1" + CR;
    public static final String HEADLIGHTS_OFF = "headlights 0" + CR;
    public static final String BRAKELIGHTS_ON = "taillights 1" + CR;
    public static final String BRAKELIGHTS_OFF = "taillights 0" + CR;
    public static final String TURN_RIGHT = "turn 2" + CR;
    public static final String TURN_LEFT = "turn 1" + CR;
    public static final String REVERSING = "turn 0" + CR;
    public static final String WIPERS_ON = "wipers 1" + CR;
    public static final String WIPERS_OFF = "wipers 0" + CR;
    public static final String STOP = "drive 0 0" + CR;
    public static final String SHUT_DOWN_ALL = HEADLIGHTS_OFF + CR + BRAKELIGHTS_OFF + CR + REVERSING + WIPERS_OFF + STOP;


    final Semaphore semaphore = new Semaphore(1);
    private SensorManager SM;
    private Sensor mySensor;
    private Bluetooth b;
    private TextView text;
    private TextView textViewSpeed;
    private RelativeLayout dashboard;
    private RelativeLayout root;
    private Button headlightsButton;
    private Button wipersButton;
    private ImageButton forwardButton;
    private ImageButton leftButton;
    private ImageButton rightButton;
    private ImageButton warningButton;
    private ImageButton needle;
    private ImageButton brakeButton;
    private RadioButton radioButton1;
    private RadioButton radioButton2;
    private RadioButton radioButton3;
    private RadioButton radioButtonR;
    private RelativeLayout command;
    private ClipData.Item gyroscope;

    private CountDownTimer timer;
    private int needleCurrentPosition;
    private int needleNextPosition = 50;
    private int i;
    private int speed = 0;
    private int direction = 1;
    private int acceleration = 1;
    private int gear = 1;
    private boolean getHeadlights = false;
    private boolean getWarning = false;
    private boolean registered = false;
    private boolean getWipers = false;
    private boolean getConnection = false;
    private String name;
    private String msg;
    private Boolean gyroscopeOn = false;
    private Boolean gyroscopeWasOn = false;
    private Float x = 0.0f;
    private Float y = 0.0f;
    private Float z = 0.0f;
    private Boolean straight = true;
    String speedSpeedometer;

    private Typeface typeface;
    private boolean dashboardHidden = false;
    private boolean timerStarted = false;
    private double engineBrake = ((speed / 500) + 1);
    //</editor-fold>

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //<editor-fold desc="Biding with activity_main">
        SM = (SensorManager) getSystemService(SENSOR_SERVICE);
        mySensor = SM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        SM.registerListener(this, mySensor, SensorManager.SENSOR_DELAY_NORMAL);

        text = (TextView) findViewById(R.id.text);
        textViewSpeed = (TextView) findViewById(R.id.textViewSpeed);
        warningButton = (ImageButton) findViewById(R.id.imageButtonWarning);
        brakeButton = (ImageButton) findViewById(R.id.imageButtonbrakePedal);
        needle = (ImageButton) findViewById(R.id.needle);
        forwardButton = (ImageButton) findViewById(R.id.imageButtonGasPedal);
        leftButton = (ImageButton) findViewById(R.id.imageButtonLeft);
        rightButton = (ImageButton) findViewById(R.id.imageButtonRight);
        dashboard = (RelativeLayout) findViewById(R.id.tableau);
        root = (RelativeLayout) findViewById(R.id.root);
        command = (RelativeLayout) findViewById(R.id.RelativeLayoutCommand);
        radioButtonR = (RadioButton) findViewById(R.id.radioButtonR);
        radioButton1 = (RadioButton) findViewById(R.id.radioButton1);
        radioButton2 = (RadioButton) findViewById(R.id.radioButton2);
        radioButton3 = (RadioButton) findViewById(R.id.radioButton3);
        headlightsButton = (Button) findViewById(R.id.buttonheadlights);
        wipersButton = (Button) findViewById(R.id.buttonWipers);
        //</editor-fold>

        //<editor-fold desc="setting onClickListener">
        warningButton.setOnClickListener(warningButtonListener);
        radioButton1.setOnClickListener(radioButton1Listener);
        radioButton2.setOnClickListener(radioButton2Listener);
        radioButton3.setOnClickListener(radioButton3Listener);
        radioButtonR.setOnClickListener(radioButtonRListener);
        headlightsButton.setOnClickListener(headlightsButtonListener);
        wipersButton.setOnClickListener(wipersButtonListener);
        //</editor-fold>

        // When the rightButton is pressed, the car turns right, when it's released the car stops turning

        rightButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                semaphore.acquireUninterruptibly();
                if (action == MotionEvent.ACTION_DOWN) {
                    msg = TURN_RIGHT;
                    b.send(msg);
                }
                if (action == MotionEvent.ACTION_UP) {
                    msg = REVERSING;
                    b.send(msg);
                }
                semaphore.release();
                return false;
            }
        });
        // When the leftButton is pressed the car turn left, when it's released the car stop turning
        leftButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                semaphore.acquireUninterruptibly();
                if (action == MotionEvent.ACTION_DOWN) {

                    msg = TURN_LEFT;
                    b.send(msg);
                }
                if (action == MotionEvent.ACTION_UP) {
                    msg = REVERSING;
                    b.send(msg);

                }
                semaphore.release();
                return false;
            }
        });

        // When the forwardButton is pressed the warnings are turned off
        forwardButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                if (action == MotionEvent.ACTION_DOWN) {
                    semaphore.acquireUninterruptibly();
                    turnOffWarning();
                    b.send(msg);
                    semaphore.release();
                }
                return false;
            }
        });
        // When the brakeButton is pressed the taillights are turned on. They are turned off when the
        // button is released
        brakeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                semaphore.acquireUninterruptibly();
                if (action == MotionEvent.ACTION_DOWN) {
                    getHeadlights = true;
                    msg = BRAKELIGHTS_ON;
                    b.send(msg);
                }
                if (action == MotionEvent.ACTION_UP) {
                    getHeadlights = false;
                    if (!getWarning) {
                        msg = BRAKELIGHTS_OFF;
                        b.send(msg);
                    }
                }
                semaphore.release();
                return false;
            }
        });


        //<editor-fold desc="setting the position of the needle">
        needle.setPivotX(16f);
        needle.setPivotY(14f);
        ObjectAnimator setNedddlePosition = ObjectAnimator.ofFloat(needle, "rotation", needleCurrentPosition, needleNextPosition);
        needleCurrentPosition = needleNextPosition;
        setNedddlePosition.setDuration(1);
        setNedddlePosition.start();
        //</editor-fold>

        //<editor-fold desc="setting the typeface of the speedmeter">
        typeface = Typeface.createFromAsset(getAssets(), "android_7.ttf");
        textViewSpeed.setTypeface(typeface);
        //</editor-fold>
//bruh
        //text.setMovementMethod(new ScrollingMovementMethod());

        //<editor-fold desc="Bluetooth initialization ">
        b = new Bluetooth(this);
        b.enableBluetooth();
        b.setCommunicationCallback(this);

        int pos;
        pos = getIntent().getExtras().getInt("pos");

        name = b.getPairedDevices().get(pos).getName();
        Display("Connecting...");
        b.connectToDevice(b.getPairedDevices().get(pos));
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mReceiver, filter);
        registered = true;
        //</editor-fold>

        dashboard.setVisibility(View.INVISIBLE);

        // Declaration of the timer:
        //  duration:500ms
        //  interval between tick : 10ms
        timer = new CountDownTimer(500, 10) {
            // onTick is being called every 10ms when the timer is started
            public void onTick(long millisUntilFinished) {
                if (speed < 0) {
                    speed = 0;
                }
                // If the brakeButton is pressed or if the phone is in brake position (gyroscope)
                if (brakeButton.isPressed() || gyroscopeBrake()) {
                    if (speed > 0) {
                        speed += DECELERATION_BRAKE;
                    }
                } else if (((forwardButton.isPressed()) || gyroscopeForward()) && !maxSpeed()) {
                    speed += acceleration;
                } else if (speed > 0) {
                    speed -= engineBrake;
                }
                //<editor-fold desc="refresh the speedmeter">
                speedSpeedometer = Integer.toString(speed / 30);
                textViewSpeed.setText(speedSpeedometer);
                //</editor-fold>
                moveNeedle();
            }

            // onFinish  is being called every 500ms, at the 'end' of the timer.
            public void onFinish() {
                semaphore.acquireUninterruptibly();

                //<editor-fold desc="if getWarning is true make taillights and headlights blink ">
                if (getWarning) {
                    if (i < 11) {
                        if (i % 2 == 0) {
                            msg = msg + BRAKELIGHTS_OFF;
                            warningButton.setBackgroundResource(R.drawable.warning);
                            msg = msg + HEADLIGHTS_OFF;

                        } else {
                            msg = msg + BRAKELIGHTS_ON;
                            msg = msg + HEADLIGHTS_ON;
                            warningButton.setBackgroundResource(R.drawable.warning_pressed);
                        }
                        i++;
                    } else {
                        getWarning = false;
                        i = 0;
                        msg = msg + BRAKELIGHTS_OFF;
                        if (getHeadlights) {
                            msg = msg + HEADLIGHTS_ON;
                        }
                    }
                }
                //</editor-fold>

                if (gyroscopeOn) {
                    gyroscopeBrakeLightsManagement();
                    gyroscopeForwardManagement();
                    gyroscopeDirectionManagement();
                }
                drive(direction, speed);
                semaphore.release();

                //if the connection is lost hide the dashboard
                if (!getConnection && !dashboardHidden) {
                    hideDashboard();
                    dashboardHidden = true;
                }
                //restart the timer
                start();
            }
        };

    }

    //turn off warnings
    private void turnOffWarning() {
        warningButton.setBackgroundResource(R.drawable.warning);
        getWarning = false;

        if (!getHeadlights) {
            msg = msg + HEADLIGHTS_OFF;
        }

        if (!brakeButton.isPressed()) {
            msg = msg + BRAKELIGHTS_OFF;
        }
    }

    // if gyroscopeOn is refreshing x,y and z values whenever the values changes
    @Override
    public void onSensorChanged(SensorEvent event) {

        if (gyroscopeOn) {
            x = (event.values[0]);
            y = event.values[1];
            z = event.values[2];
        }

    }

    // This method is mandatory
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    // When the wipersButton has been clicked if the wipers are on, turn them off and back
    private View.OnClickListener wipersButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            semaphore.acquireUninterruptibly();

            if (getWipers) {
                msg = msg + WIPERS_OFF;
                getWipers = false;
                wipersButton.setBackgroundResource(R.drawable.essuie_glace_off);

            } else {
                msg = msg + WIPERS_ON;
                getWipers = true;
                wipersButton.setBackgroundResource(R.drawable.essuie_glace_on);

            }
            b.send(msg);
            semaphore.release();
        }
    };

    // When the headlightsButton has been clicked if the headlights are on ,turn them off and back
    private View.OnClickListener headlightsButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            semaphore.acquireUninterruptibly();

            if (getHeadlights) {
                msg = HEADLIGHTS_OFF;
                getHeadlights = false;
                headlightsButton.setBackgroundResource(R.drawable.phares_off);

            } else {
                msg = HEADLIGHTS_ON;
                getHeadlights = true;
                headlightsButton.setBackgroundResource(R.drawable.phares_on);
            }

            b.send(msg);
            semaphore.release();
        }
    };

    // When the radioButton1 has been pressed set the relevant parameters and call drive with the new
    // parameters
    private View.OnClickListener radioButton1Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            gear = 1;
            acceleration = 1;
            direction = 1;

            if (speed > 1000)
                speed = 1000;

            drive(direction, speed);
        }
    };


    // When the radioButton2 has been pressed set the relevant parameters and call drive with the
    // new parameters
    private View.OnClickListener radioButton2Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (speed > 2000)
                speed = 2000;

            gear = 2;
            acceleration = 5;
            direction = 1;
            drive(direction, speed);
        }
    };
    // When the radioButton3 has been pressed set the relevant parameters and call drive with the new
    // parameters
    private View.OnClickListener radioButton3Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            gear = 3;
            acceleration = 10;
            direction = 1;
            drive(direction, speed);
        }
    };

    // When the radioButtonR has been pressed set the relevant parameters and call drive with the new parameters
    private View.OnClickListener radioButtonRListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (speed > 1000)
                speed = 1000;

            gear = 1;
            acceleration = 1;
            direction = 2;
        }
    };

    // When the warningButton has been clicked, if the warning were off, stop the car and set
    // getWarning true.
    // if warning were on turn them off and set getWarning false
    private View.OnClickListener warningButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            semaphore.acquireUninterruptibly();
            if (!getWarning) {

                msg = STOP;
                speed = 0;
                b.send(msg);
                getWarning = true;

            } else {
                getWarning = false;

                if (!getHeadlights) {
                    msg = BRAKELIGHTS_OFF;
                }

                if (!brakeButton.isPressed()) {
                    msg = msg + BRAKELIGHTS_OFF;
                }

                b.send(msg);
                getWarning = false;
                warningButton.setBackgroundResource(R.drawable.warning);
            }
            semaphore.release();
        }
    };

    // moves the needle to the next position
    private void moveNeedle() {
        needleNextPosition = (int) ((speed / (10 * gear) * 2.5) + 50);
        ObjectAnimator animationAiguille = ObjectAnimator.ofFloat(needle, "rotation", needleCurrentPosition, needleNextPosition);
        needleCurrentPosition = needleNextPosition;
        animationAiguille.setDuration(1);
        animationAiguille.start();
    }

    // When this function is called the car move in the direction and at the specified speed + send
    // msg as it was set and reset it afterwards
    // in :  int speed: the speed of the engine
    //    :  int sens : the sens the car will move (backward, forward,none)
    private void drive(int sens, int speed) {
        msg = msg + "drive " + String.valueOf(sens) + ' ' + String.valueOf(speed) + CR;
        b.send(msg);
        msg = "";
    }

    // Return true if the phone is in brake position (gyroscope need to be on)
    private boolean gyroscopeBrake() {
        Boolean toReturn = false;
        if (z < 0) {
            toReturn = true;
        }
        return toReturn;
    }

    // Return true if the car is at it's max speed.
    // It's max speed depends on the engine's speed and the current gear.

    private boolean maxSpeed() {
        Boolean toReturn = true;

        if (speed / (10 * gear) < 100) {
            toReturn = false;
        }

        return toReturn;
    }

    //Return true if the phone is in forward position(gyroscope need to be on)
    private boolean gyroscopeForward() {
        Boolean toReturn = false;
        if (z > 8) {
            toReturn = true;
        }
        return toReturn;
    }

    //Move the dashboard from underneath the the layout to the top.
    //Move text if needed(deBug off).
    private void liftDashboard() {
        if (isDebug) {
            ObjectAnimator liftDashboard = ObjectAnimator.ofFloat(dashboard, "translationY",
                    dashboard.getHeight(), 0);
            liftDashboard.setDuration(1000);
            liftDashboard.start();

        } else {
            ObjectAnimator liftTextNoDebug = ObjectAnimator.ofFloat(text, "translationY",
                    0, -text.getHeight());
            liftTextNoDebug.setDuration(1000);
            liftTextNoDebug.start();

            ObjectAnimator liftDashboardNoDebug = ObjectAnimator.ofFloat(dashboard, "translationY",
                    root.getHeight(), -text.getHeight());
            liftDashboardNoDebug.setDuration(1500);
            liftDashboardNoDebug.start();
        }
    }


    // Move the dashboard from the top to underneath the the layout.
    //Lower the text so it can be  visible
    private void hideDashboard() {
        ObjectAnimator lowerDashboard = ObjectAnimator.ofFloat(dashboard, "translationY", 0, dashboard.getHeight());
        lowerDashboard.setDuration(1000);
        lowerDashboard.start();

        ObjectAnimator lowerText = ObjectAnimator.ofFloat(text, "translationY", -text.getHeight(), 0);
        lowerText.setDuration(1000);
        lowerText.start();
    }

    //Lower the root Layout to the heights of the text  for the text to be visible
    private void lowerText() {
        ObjectAnimator lowerText1 = ObjectAnimator.ofFloat(text, "translationY", -text.getHeight(), 0);
        lowerText1.setDuration(1000);
        lowerText1.start();

        ObjectAnimator lowerText2 = ObjectAnimator.ofFloat(dashboard, "translationY", -text.getHeight(), 0);
        lowerText2.setDuration(1000);
        lowerText2.start();
    }

    //Rize the root Layout to the heights of the text  for the text to be uppermost the layout
    private void rizeText() {
        ObjectAnimator rizeText1 = ObjectAnimator.ofFloat(text, "translationY", 0, -text.getHeight());
        rizeText1.setDuration(1000);
        rizeText1.start();

        ObjectAnimator rizeText2 = ObjectAnimator.ofFloat(dashboard, "translationY", 0, -text.getHeight());
        rizeText2.setDuration(1000);
        rizeText2.start();
    }


    //Turn on the brakelight if the phone is in brake position(gyroscope need to be on)
    private void gyroscopeBrakeLightsManagement() {
        if (gyroscopeBrake()) {
            getHeadlights = true;
            msg = BRAKELIGHTS_ON;
            b.send(msg);

        } else {
            getHeadlights = false;

            if (!getWarning) {
                msg = BRAKELIGHTS_OFF;
                b.send(msg);
            }
        }
    }

    //Turn off the warning if the phone is in forward position (gyroscope need to be on)
    private void gyroscopeForwardManagement() {
        if (gyroscopeForward()) {
            turnOffWarning();
        }
    }

    //When the phone is  in the right position the car turn right,
    //When the phone is  in the left position the car turn left,
    //When the phone is  in neither of them the car co straight,
    private void gyroscopeDirectionManagement() {
        if (x > 5) {
            msg = TURN_LEFT;
            straight = false;

        } else if (x < -5) {
            msg = TURN_RIGHT;
            straight = false;

        } else if (!straight) {
            msg = REVERSING;
            straight = true;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (registered) {
            unregisterReceiver(mReceiver);
            registered = false;
        }
    }

    //stop when the app isn't focus
    public void onStop() {
        warningButton.setBackgroundResource(R.drawable.warning);
        headlightsButton.setBackgroundResource(R.drawable.phares_off);
        wipersButton.setBackgroundResource(R.drawable.essuie_glace_off);
        speed = 0;
        super.onStop();
        semaphore.acquireUninterruptibly();
        msg = SHUT_DOWN_ALL;
        b.send(msg);
        semaphore.release();
        if (gyroscopeOn) {
            gyroscopeWasOn = true;
        } else {
            gyroscopeWasOn = false;
        }
        gyroscopeOn = false;
        resetGyroscopeData();
    }

    //restart the app when the app recover the focus
    public void onResume() {
        if (gyroscopeWasOn) {
            gyroscopeOn = true;
        }
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    //reset x,y and z parameter
    public void resetGyroscopeData() {
        z = 0.0f;
        y = 0.0f;
        z = 0.0f;
    }

    private void hideCommand() {
        ObjectAnimator hideCommand = ObjectAnimator.ofFloat(command, "translationY", 0, command.getHeight());
        hideCommand.setDuration(1000);
        hideCommand.start();
    }

    private void showCommand() {
        ObjectAnimator showCommand = ObjectAnimator.ofFloat(command, "translationY", command.getHeight(), 0);
        showCommand.setDuration(1000);
        showCommand.start();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.close:
                b.removeCommunicationCallback();
                b.disconnect();
                Intent intent = new Intent(this, Select.class);
                startActivity(intent);
                finish();

                return true;

            case R.id.rate:
                Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
                }
                return true;

            case R.id.deBug:
                if (item.isChecked()) {
                    item.setChecked(false);
                    rizeText();
                    isDebug = false;
                } else {
                    item.setChecked(true);
                    lowerText();
                    isDebug = true;
                }
                return true;

            case R.id.about:
                startActivity(new Intent(Chat.this, Pop.class));
                return true;
            case R.id.gyroscope:
                PackageManager packageManager = getPackageManager();
                boolean gyroExists = packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);
                if (!gyroExists){
                    item.setEnabled(false);
                    Toast.makeText(Chat.this, "Your device dosn't have gyroscope", Toast.LENGTH_LONG).show();
                }else if (item.isChecked()) {
                    item.setChecked(false);
                    resetGyroscopeData();
                    item.setChecked(false);
                    gyroscopeOn = false;
                    showCommand();
                } else {
                    item.setChecked(true);
                    gyroscopeOn = true;
                    hideCommand();

                }



                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void Display(final String s) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.append(s + "\n");
                text.append(s + "\n");
            }
        });
    }

    @Override
    public void onConnect(BluetoothDevice device) {
        Display("Connected to " + device.getName() + " - " + device.getAddress());
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                getConnection = true;
                dashboard.setVisibility(View.VISIBLE);
                if (!timerStarted) {
                    timer.start();
                }

                liftDashboard();
            }
        });
    }

    @Override
    public void onDisconnect(BluetoothDevice device, String message) {

        if (getConnection) {
            getConnection = false;
        }
        Display("Disconnected!");
        Display("Connecting again...");
        b.connectToDevice(device);
    }

    @Override
    public void onMessage(String message) {
        Display(name + ": " + message);
    }

    @Override
    public void onError(String message) {
        Display("Error: " + message);
    }

    @Override
    public void onConnectError(final BluetoothDevice device, String message) {
        Display("Error: " + message);
        Display("Trying again in 3 sec.");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        b.connectToDevice(device);
                    }
                }, 2000);
            }
        });
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                Intent intent1 = new Intent(Chat.this, Select.class);

                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        if (registered) {
                            unregisterReceiver(mReceiver);
                            registered = false;
                        }
                        startActivity(intent1);
                        finish();
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        if (registered) {
                            unregisterReceiver(mReceiver);
                            registered = false;
                        }
                        startActivity(intent1);
                        finish();
                        break;
                }
            }
        }
    };
}