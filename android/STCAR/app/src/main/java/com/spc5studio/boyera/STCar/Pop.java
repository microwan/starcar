package com.spc5studio.boyera.STCar;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;

import me.aflak.STCar.R;
//This class hundle the popupwindow
public class Pop extends Activity {

    Button buttonVer;
    Button buttonBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popupwindow);
        buttonVer = (Button) findViewById(R.id.button3);
        buttonVer.setOnClickListener(buttonBackListener);
        buttonBack = (Button) findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(buttonBackListener);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int) (width), (int) (height));
    }


    public void browserYoutube(View view) {
        Intent browserintentYoutube = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=4ftBosYKEIE"));
        startActivity(browserintentYoutube);

    }

    public void browserST(View view) {
        Intent browserintentST = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.st.com/content/st_com/en.html"));
        startActivity(browserintentST);

    }

    public void browserSpc5Studio(View view) {
        Intent browserintentSpc5 = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.st.com/en/development-tools/spc5-studio.html"));
        startActivity(browserintentSpc5);

    }

    private View.OnClickListener buttonBackListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}
